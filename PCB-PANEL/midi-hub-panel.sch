EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Oxi One Prototype"
Date "2021-01-14"
Rev "4"
Comp "Manuel Vazquez Rodriguez"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 8175 1425 0    118  ~ 24
MOUNTING HOLES
$Comp
L Mechanical:MountingHole H4
U 1 1 608E91B9
P 8300 2575
F 0 "H4" H 8400 2621 50  0000 L CNN
F 1 "MountingHole" H 8400 2530 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965" H 8300 2575 50  0001 C CNN
F 3 "~" H 8300 2575 50  0001 C CNN
F 4 "-" H 8300 2575 50  0001 C CNN "LCSC Part"
F 5 "-" H 8300 2575 50  0001 C CNN "Package"
F 6 "-" H 8300 2575 50  0001 C CNN "Ref. Other"
	1    8300 2575
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 608E8C34
P 8300 2375
F 0 "H3" H 8400 2421 50  0000 L CNN
F 1 "MountingHole" H 8400 2330 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965" H 8300 2375 50  0001 C CNN
F 3 "~" H 8300 2375 50  0001 C CNN
F 4 "-" H 8300 2375 50  0001 C CNN "LCSC Part"
F 5 "-" H 8300 2375 50  0001 C CNN "Package"
F 6 "-" H 8300 2375 50  0001 C CNN "Ref. Other"
	1    8300 2375
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 608E8765
P 8300 2175
F 0 "H2" H 8400 2221 50  0000 L CNN
F 1 "MountingHole" H 8400 2130 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965" H 8300 2175 50  0001 C CNN
F 3 "~" H 8300 2175 50  0001 C CNN
F 4 "-" H 8300 2175 50  0001 C CNN "LCSC Part"
F 5 "-" H 8300 2175 50  0001 C CNN "Package"
F 6 "-" H 8300 2175 50  0001 C CNN "Ref. Other"
	1    8300 2175
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 666CE83D
P 8300 1975
F 0 "H1" H 8400 2021 50  0000 L CNN
F 1 "MountingHole" H 8400 1930 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965" H 8300 1975 50  0001 C CNN
F 3 "~" H 8300 1975 50  0001 C CNN
F 4 "-" H 8300 1975 50  0001 C CNN "LCSC Part"
F 5 "-" H 8300 1975 50  0001 C CNN "Package"
F 6 "-" H 8300 1975 50  0001 C CNN "Ref. Other"
	1    8300 1975
	1    0    0    -1  
$EndComp
$EndSCHEMATC
